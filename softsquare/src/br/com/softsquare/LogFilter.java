package br.com.softsquare;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

public class LogFilter implements Filter{

	@Override
	public void destroy() {
		System.out.println("Finalizando metodo destroy");
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
			throws IOException, ServletException {
		String ipAddress = request.getRemoteAddr();
		System.out.println("IP :"+ ipAddress);
	}

	@Override
	public void init(FilterConfig filterConfig) throws ServletException {
		String testParam = filterConfig.getInitParameter("test-param");
		System.out.println("Inicialização do Filter");
	}

}
