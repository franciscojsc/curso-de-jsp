<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Cockies Read</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body >
		
		<%@include file="includes/header.jsp" %>
		
		<%@include file="includes/menu.jsp" %>
		
		<div id="wrapper">
			<main>
				<div id="content">
					<div class="innertube">
						<h1>Cockies Read</h1>
						
						<%
						
						Cookie cookie = null;
						Cookie [] cookies = null;
						
						cookies = request.getCookies();
						
						if(cookies!=null) {
							for(int i = 0; i< cookies.length; i++){
								cookie = cookies[i];
								out.println("<br/>Nome "+cookie.getName());
								out.println("<br/>Valor "+cookie.getValue());
							}
						}
						
						
						%>
						
					</div>		
				</div>
			</main>
		</div>
				
		<%@include file="includes/footer.jsp" %>
	
	
	</body>
</html>