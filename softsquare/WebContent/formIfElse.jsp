<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Formulario Se Entao</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
	
		<%@include file="includes/header.jsp" %>
		<%@include file="includes/menu.jsp" %>
	
		<div id="wrapper">
				<main>
					<div id="content">
						<div class="innertube">
						
							<h1>Formulario Se Entao</h1>
							
							<h3> De acordo com paramento apos o input a pagina 
							 seguinte responde de acordo com a escolha</h3>
							
							<br/>
							
							<ul>
								<li>Digite SIM</li>
								<li>Digite NAO</li>
							</ul>
							
							<br/>
							
							<form method="post"   action="SeEntaoReceber.jsp">
							<input type ="text" name="escolha" id="valor">
							<br/>
							<input type ="submit" name="enviar">
								
							</form>
						
						</div>		
					</div>
				</main>
			</div>
			
	
		<%@include file="includes/footer.jsp" %>
	
	</body>
</html>