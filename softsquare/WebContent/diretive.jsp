<%@taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@page errorPage="error/404.jsp" %>
<%@page isErrorPage="true" %>
<%@ page import="java.util.*" %>
<%@ page buffer="16kb" %>
<%@ page info="Desenvolvido por Francisco Chaves" %>

<!-- encoding -->
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>


<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>diretivas</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body >
		
		<%@include file="includes/header.jsp" %>
		
		<%@include file="includes/menu.jsp" %>
		
		<div id="wrapper">
			<main>
				<div id="content">
					<div class="innertube">
						<h1>Diretivas</h1>
						
						<c:out value="Olá Mundo Jstl"></c:out>
						
					</div>		
				</div>
			</main>
		</div>
				
		<%@include file="includes/footer.jsp" %>
	
	
	</body>
</html>