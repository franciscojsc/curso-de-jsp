<%@page import="java.util.Enumeration"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Requisicao do Cliente</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body >
		
		<%@include file="includes/header.jsp" %>
		
		<%@include file="includes/menu.jsp" %>
		
		<div id="wrapper">
			<main>
				<div id="content">
					<div class="innertube">
						<h1>Requisicao do Cliente</h1>
						
						<table width="100%" border="1" align="center">
							<tr>
								<td><h2>Header Nomes</h2></td>
								<td><h2>HeaderValores</h2></td>
							</tr>
							
							<%
								//Informacoes da requisicao do cliente
								Enumeration hearderNames = request.getHeaderNames();
							
								while(hearderNames.hasMoreElements()) {
									
									String paramNames = (String) hearderNames.nextElement();
									out.print("<tr><td>"+paramNames+"</td>");
									String paramValue = request.getHeader(paramNames);
									out.print("<td>"+paramValue+"</td></tr>");
								}
							
							%>
						
						</table>
						
					</div>		
				</div>
			</main>
		</div>
				
		<%@include file="includes/footer.jsp" %>
	
	
	</body>
</html>