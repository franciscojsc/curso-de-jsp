<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Switch</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body >
		
		<%@include file="includes/header.jsp" %>
		
		<%@include file="includes/menu.jsp" %>
		
		<div id="wrapper">
			<main>
				<div id="content">
					<div class="innertube">
						<h1>Switch</h1>
						
						<%
							int dia = 7;
						
							switch(dia){
							case 1:out.println("Domingo");
							break;
							case 2:out.println("Segunda-feira");
							break;
							case 3:out.println("Terca-feira");
							break;
							case 4:out.println("Quarta-feira");
							break;
							case 5:out.println("Quinta-feira");
							break;
							case 6:out.println("Sexta-feira");
							break;
							case 7:out.println("Sabado");
							break;
							default:
								out.println("Nao encontrado");
							}
						%>
						
					</div>		
				</div>
			</main>
		</div>
				
		<%@include file="includes/footer.jsp" %>
	
	
	</body>
</html>