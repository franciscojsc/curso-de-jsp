<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Objetos implicitos</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body >
		
		<%@include file="includes/header.jsp" %>
		
		<%@include file="includes/menu.jsp" %>
		
		<div id="wrapper">
			<main>
				<div id="content">
					<div class="innertube">
						<h1>Objetos implicitos</h1>
						
						<%
						System.out.println(request.getLocalAddr());
						System.out.println(request.getPathTranslated());
						System.out.println(request.getLocalName());
						System.out.println(request.getProtocol());
						System.out.println(request.getAuthType());
						System.out.println(request.getPathInfo());
						System.out.println(request.getContextPath());
						System.out.println(request.getLocalPort());
						System.out.println(request.getLocale());
						System.out.println(pageContext.getPage());
						System.out.println(request.getRemoteUser());
						System.out.println(request.getServletPath());
						System.out.println(request.getRequestURI());
						
						%>
						
					</div>		
				</div>
			</main>
		</div>
				
		<%@include file="includes/footer.jsp" %>
	
	
	</body>
</html>