<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Cockies Input</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body >
		
		<%@include file="includes/header.jsp" %>
		
		<%@include file="includes/menu.jsp" %>
		
		<div id="wrapper">
			<main>
				<div id="content">
					<div class="innertube">
						<h1>Cockies Input</h1>
						
						<table>
							<form action="getcockie.jsp" method="post">
								<tr><td>Nome:<input type="text" id="nome" name="nome"/></td></tr>
								<tr><td>Sobrenome:<input type="text" id="sobrenome" name="sobrenome"/></td></tr>
								<tr><td><input type="submit" value="Salvar"/></td></tr>
							</form>
						</table>
					</div>		
				</div>
			</main>
		</div>
				
		<%@include file="includes/footer.jsp" %>
	
	
	</body>
</html>