<%@page import="org.apache.jasper.tagplugins.jstl.core.Catch"%>
<%@ page language="java" %>
    
 <!-- encoding -->
<%@ page contentType="text/html; charset=UTF-8" %>
<%@ page pageEncoding="UTF-8" %>
<%@ page import="java.sql.*" %> 

<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Index JSP</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<script  type="text/javascript" src="js/script.js"> </script>
	</head>
	<body >
		
		<%@include file="includes/header.jsp" %>
		
		<%@include file="includes/menu.jsp" %>
		
		<div id="content">
			<div class="innertube">
				<h1>Contéudo</h1>
				<div id="divFrame" 	style="width: 80%; height:500px; border: 1px solid #d1d1d1; margin:5% auto;"> </div>
				
			</div>		
		</div>
		
					
		<%@include file="includes/footer.jsp" %>
	
	
	</body>
</html>