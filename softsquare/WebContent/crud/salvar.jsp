<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Salvar</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<style type="text/css">
			table {
				border-style:none;
				}
			td {
				border-style:none;
				}
		</style>
	</head>
	<body >
		
		<%@include file="includes/header.jsp" %>
		
		<%@include file="includes/menu.jsp" %>
		
		<div id="content">
			<div class="innertube">
				<h1>Salvar no Banco de Dados</h1>
				<form method="get" action="realizaSave.jsp">
					<table align="center"  >
						<tr><td><p>Digite o c�digo</p> </td><td> <input type="text" id="id" name="id"  placeholder="digite aqui o id"/></td></tr>
						<tr><td><p>Digite o nome</p> </td><td> <input type="text" id="name" name="name"  placeholder="digite aqui o nome"/></td></tr>
						<tr><td><p>Digite o Estado</p> </td><td> <input type="text" id="city" name="city"  placeholder="digite aqui o estado"/></td></tr>
						<tr><td><p>Digite o telefone</p> </td><td> <input type="text" id="phone" name="phone"  placeholder="digite aqui o telefone"/></td></tr>
						<tr><td><p style="font-size:30px;">Salvar</p> </td><td> <input type="submit" value="salvar"/></td></tr>
					</table>
				</form>
				
			</div>		
		</div>
		
		<%@include file="includes/footer.jsp" %>
	
	
	</body>
</html>