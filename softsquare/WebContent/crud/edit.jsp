<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ page import="java.sql.*" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Edit</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link href="https://fonts.googleapis.com/css?family=Supermercado+One" rel="stylesheet"> 
	</head>
	<body >
		
		<%@include file="includes/header.jsp" %>
		
		<%@include file="includes/menu.jsp" %>
		
				<div id="content">
					<div class="innertube">
						<h1>Editar</h1>
						
					
						
						<table align="center" >
							<tr align="center" valign="middle">
								<td>ID</td>
								<td>NOME</td>
								<td>ESTADO</td>
								<td>TELEFONE</td>
							</tr>
							<%
							
							
							out.print("<form action='realizaUpdate.jsp' method='get'>"); 	
							
							
							try{
								
								
								Class.forName("com.mysql.jdbc.Driver").newInstance();
								Connection con = null;
								ResultSet rs = null;
								PreparedStatement st = null;
								
								
								//receber o valor, passado na requisição
								String valorParam = request.getParameter("id");
								
								String sql = "Select * from student where id = "+ valorParam;
								
								String url="jdbc:mysql://localhost:33063/sofsquare?autoReconnect=true&useSSL=false";
								String user="root";
								String senha="1234";
								
								con =  DriverManager.getConnection(url, user,senha);
								st = con.prepareStatement(sql);		
								rs = st.executeQuery();
								
								
								
									while(rs.next()){
										out.print("<tr>");
											out.print("<td>");
												out.println(rs.getString(1));
												
												//campo invisivel para armazenamento temporario do valor que sera passado na requisição
												out.println("<input type='hidden' name='id' id='id' value='"+rs.getString(1)+"'/>");
											out.print("</td>");
											
											out.print("<td>");
												out.println("<input type='text' name='name' id='name' value='"+rs.getString(2)+"'/>");
											out.print("</td>");
											
											out.print("<td>");
												out.println("<input type='text' name='city' id='city' value='"+rs.getString(3)+"'/>");
											out.print("</td>");
											
											out.print("<td>");
												out.println("<input type='text' name='phone' id='phone' value='"+rs.getString(4)+"'/>");
											out.print("</td>");
											
											out.print("<td>");
												out.println("<input type='submit' value='atualizar'>");
												
											out.print("</td>");
										out.print("</tr>");
									}
									
								out.print("</from>");
								
								rs.close();
								st.close();
								con.close();
								
							}
							catch(SQLException  e){
									out.print("erro "+e);
							}catch(ClassNotFoundException e){
									out.print("erro "+e);
							}
							%>
						</table>
					
			</div>		
		</div>
		
					
		<%@include file="includes/footer.jsp" %>
	
	
	</body>
</html>