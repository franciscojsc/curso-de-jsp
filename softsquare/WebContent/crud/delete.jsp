<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
    
<%@ page import="java.sql.*" %> 
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Delete</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<link href="https://fonts.googleapis.com/css?family=Supermercado+One" rel="stylesheet"> 
	</head>
	<body >
		
		<%@include file="includes/header.jsp" %>
		
		<%@include file="includes/menu.jsp" %>
		
		<div id="content">
			<div class="innertube">
				<h1>Lista do Banco de Dados - DELETE</h1>
				
					<table align="center" >
						<tr align="center" valign="middle">
							<td>ID</td>
							<td>NOME</td>
							<td>ESTADO</td>
							<td>TELEFONE</td>
							<td >A��ES</td>
						</tr>
						<%
						
						try{
							
							
							Class.forName("com.mysql.jdbc.Driver").newInstance();
							Connection con = null;
							ResultSet rs = null;
							PreparedStatement st = null;
							
							String sql = "Select * from student";
							
							String url="jdbc:mysql://localhost:33063/sofsquare?autoReconnect=true&useSSL=false";
							String user="root";
							String senha="1234";
							
							con =  DriverManager.getConnection(url, user,senha);
							st = con.prepareStatement(sql);		
							rs = st.executeQuery();
							
							
								while(rs.next()){
									out.print("<tr>");
										out.print("<td>");
											out.println(rs.getString(1));
										out.print("</td>");
										
										out.print("<td>");
											out.println(rs.getString(2));
										out.print("</td>");
										
										out.print("<td>");
											out.println(rs.getString(3));
										out.print("</td>");
										
										out.print("<td>");
											out.println(rs.getString(4));
										out.print("</td>");
										
										out.print("<td>");
											out.println("<a href='realizarDelete.jsp?id="+rs.getString(1)+"'>excluir</a>");
										out.print("</td>");
									
									out.print("</tr>");
								}
						
							rs.close();
							st.close();
							con.close();
							
						}
						catch(SQLException  e){
								out.print("erro "+e);
						}catch(ClassNotFoundException e){
								out.print("erro "+e);
						}
						%>
					</table>
			</div>		
		</div>
				
		<%@include file="includes/footer.jsp" %>
	
	
	</body>
</html>