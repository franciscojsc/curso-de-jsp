<%@page import="javax.mail.internet.InternetAddress"%>
<%@page import="javax.mail.internet.MimeMessage"%>
<%@ page import="javax.mail.*" %>
<%@ page import="javax.naming.*" %>
<%@ page import="java.io.*" %>
<%@ page import="javax.activation.*" %>
<%@ page import="com.sun.mail.smtp.*" %>
<%@ page import="java.util.*" %>

<!DOCTYPE unspecified PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<body>
		<%
		String user, host, pass,msg;
		host = "smtp.gmail.com";
		pass="senha";
		user="seuEmail@gmail.com";
		msg= "Testando o envio de email atraves do java";
		String SSL_FACTORY = "javax.net.ssl.SSLSocketFactory";
		String to = user;
		String from = user;
		String subject = "Enviando arquivos anexo";
		String messageText = msg;
		String fileAttachment = "C:/Users/Francisco/workspace/softsquare/WebContent/teste-email.txt";
		
		boolean WasEmailSent;
		boolean sessionDebug = true;
		
		Properties props = System.getProperties();
		props.put("mail.host", host);
		props.put("mail.transport.protocol.", "smtp");
		props.put("mail.smtp.auth","true" );
		props.put("mail.smtp.","true" );
		props.put("mail.smtp.port","465" );
		props.put("mail.smtp.startls.enable","true" );
		props.put("mail.smtp.socketFactory.fallback","false" );
		props.put("mail.smtp.socketFactory.class",SSL_FACTORY );
		
		Session mailSession = Session.getDefaultInstance(props, null);
		mailSession.setDebug(sessionDebug);
		Message message = new MimeMessage(mailSession);
		message.setFrom(new InternetAddress(from));
		InternetAddress[] addresses = {new InternetAddress(to)};
		message.setRecipients(Message.RecipientType.TO, addresses);
		message.setSubject(subject);
		message.setContent(messageText, "text/html");
		
		DataSource source = new FileDataSource(fileAttachment);
		message.setDataHandler(new DataHandler(source));
		message.setFileName("Anexo.txt");
		
		Transport transport = mailSession.getTransport("smtp");
		
		try{
			
			out.print("Enviado com sucesso!!");
			transport.connect(host, user, pass);
			transport.sendMessage(message, message.getAllRecipients());
			WasEmailSent= true;		
			
		}catch(Exception e){
			WasEmailSent = false;
		}finally{
			transport.close();
		}
		
		%>

	</body>
</html>