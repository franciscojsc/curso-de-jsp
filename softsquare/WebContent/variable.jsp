<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Variaveis</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body>
	
	<%@include file="includes/header.jsp" %>
		
		<%@include file="includes/menu.jsp" %>
		
		<div id="wrapper">
			<main>
				<div id="content">
					<div class="innertube">
						<h1>Variaveis</h1>
						
						<!-- Variaveis -->
						<%
						
						String texto = "texto de \n string!!!";
						int inteiro = 1;
						boolean bol = true;
						char a = 1;
						float f = 10.25f;
						double d = 4.5;
						long l = 54545454545l;
						byte b = 0010;
						%>
						
						<table border =1>
							<tr>
								<tr><td>String</td><td><% out.println(texto);%></td></tr>
								
								<tr><td>inteiro</td><td><%out.println(inteiro);%></td></tr>
								
								<tr><td>boolean</td><td><%out.println(bol);%></td></tr>
								
								<tr><td>char</td><td><%out.println(a);%></td></tr>
								
								<tr><td>float</td><td><%out.println(f);%></td></tr>
								
								<tr><td>double</td><td><%out.println(d);%></td></tr>
								
								<tr><td>long</td><td><%out.println(l);%></td></tr>
								
								<tr><td>byte</td><td><%out.println(b);%></td></tr>
								
							</tr>
							
						</table>
						
						
					</div>		
				</div>
			</main>
		</div>
					
		<%@include file="includes/footer.jsp" %>
		
	</body>
</html>