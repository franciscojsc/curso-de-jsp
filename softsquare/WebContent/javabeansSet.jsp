<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@page import="br.com.softsquare.Person" %>
  
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		<title>Java Beans Set</title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body >
		
		<%@include file="includes/header.jsp" %>
		
		<%@include file="includes/menu.jsp" %>
		
		<div id="wrapper">
			<main>
				<div id="content">
					<div class="innertube">
						<h1>Java Beans Set</h1>
						
						<!-- criar um identificado para o Objeto Person -->
						<jsp:useBean id="pessoa" class="br.com.softsquare.Person"></jsp:useBean>
						
						<!-- atribuindo um valor no artibuto nome de Person -->
						<jsp:setProperty property="nome" name="nome"/>
						
						
						
						
						
						<table>
							<form action="javabeansGet.jsp" method="post">
								<tr>
									<td>Nome:</td>
									<td><input type="text" name="nome"/></td>
								</tr>
								<tr>
									<td>Sobrenome:</td>
									<td><input type="text" name="sobrenome"/></td>
								</tr>
								<tr>
									<td>Idade:</td>
									<td><input type="text" name="idade"/></td>
								</tr>
								<tr>
									<td>Enviar:</td>
									<td><input type="submit" name="enviar" /></td>
								</tr>
							
							</form>
						</table>
					</div>		
				</div>
			</main>
		</div>
				
		<%@include file="includes/footer.jsp" %>
	
	
	</body>
</html>