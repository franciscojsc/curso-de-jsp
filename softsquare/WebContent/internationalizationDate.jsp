<%@page import="javax.swing.text.DateFormatter"%>
<%@page import="java.text.DateFormat"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ page import="java.io.*, java.util.Locale, javax.servlet.*, javax.servlet.http.*, java.util.Date.*, java.text.DateFormat.*, java.util.*" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
	<head>
		<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
		
		<%

			Locale locale = request.getLocale();
			String date = DateFormat.getDateTimeInstance(DateFormat.FULL, DateFormat.SHORT,locale).format(new Date());
			
			String title = "Locale specifc dates";	
		%>
		
		<title> <%out.print(title);%> </title>
		<link rel="stylesheet" type="text/css" href="css/style.css">
	</head>
	<body >
		
		<%@include file="includes/header.jsp" %>
		
		<%@include file="includes/menu.jsp" %>
		
		<div id="wrapper">
			<main>
				<div id="content">
					<div class="innertube">
						<h1>
						<br/>
						<%
							out.print(title);
						%>
												
						</h1>
						<br/>
						
						Local date : <% out.print(date); %>
						
						
					</div>		
				</div>
			</main>
		</div>
				
		<%@include file="includes/footer.jsp" %>
	
	
	</body>
</html>