use softsquare;

Create table if not exists 'Student'(
	id int(11)
	,name varchar(200)
	,city varchar(100)
	,phone varchar(20)
);

insert into student (id, name, city, phone) values (1, 'Francisco', 'Sergipe', '79-98989-9876');
insert into student (id, name, city, phone) values (2, 'Francisco', 'Sergipe', '79-98989-9876');
insert into student (id, name, city, phone) values (3, 'Francisco', 'Sergipe', '79-98989-9876');
insert into student (id, name, city, phone) values (4, 'Francisco', 'Sergipe', '79-98989-9876');
insert into student (id, name, city, phone) values (5, 'Francisco', 'Sergipe', '79-98989-9876');
insert into student (id, name, city, phone) values (6, 'Francisco', 'Sergipe', '79-98989-9876');
insert into student (id, name, city, phone) values (7, 'Francisco', 'Sergipe', '79-98989-9876');
insert into student (id, name, city, phone) values (8, 'Francisco', 'Sergipe', '79-98989-9876');
insert into student (id, name, city, phone) values (9, 'Francisco', 'Sergipe', '79-98989-9876');
insert into student (id, name, city, phone) values (10, 'Francisco', 'Sergipe', '79-98989-9876');
insert into student (id, name, city, phone) values (11, 'Francisco', 'Sergipe', '79-98989-9876');
insert into student (id, name, city, phone) values (12, 'Francisco', 'Sergipe', '79-98989-9876');